from pathlib import Path

import zipfile
import sys
import configparser
import os
from functions import get_last_tag

EXCLUDE = ()


def iterfiles(path: Path, top=False):
    """Recursive file iterator with exclude list"""

    if top:
        yield path

    for child in path.iterdir():
        child: Path
        if child.name not in EXCLUDE:
            if child.is_dir():
                yield child
                for file in iterfiles(child):
                    yield file
            else:
                yield child


def bump_version(source: Path):
    """Updates the metadata file with release tag"""
    version = get_last_tag()

    file: Path = source / "metadata.txt"
    if not file.exists():
        raise FileNotFoundError(f"Can't find metadata.txt file: {file.absolute()}")
    config = configparser.ConfigParser()
    config.read(file)
    config["general"]["version"] = version
    with file.open(mode="w") as fp:
        config.write(fp)


def create_zip(source: Path, filename="release"):
    target = source.parent / "assets" / f"{filename}.zip"
    target.parent.mkdir(parents=True, exist_ok=True)
    print(f"Creating source from: {source.resolve()}")
    print(f"Creating zip to: {target.resolve()}")

    files = source
    print(files.absolute())
    print(f"Writing file {target.name}")
    with zipfile.ZipFile(str(target), mode="w") as zf:
        for file in iterfiles(files, top=True):
            zf.write(str(file), str(filename / file.relative_to(files)), zipfile.ZIP_DEFLATED)

    print(f"Finished writing file")


if __name__ == "__main__":
    source = Path(__file__).parents[1] / "gtt"
    bump_version(source)
    create_zip(source, filename=sys.argv[1])
