
## TODO
* [x] Create ROI
* [x] Allow ROI sample creation / surface creation
* [x] Sync surface class names to type integers
* [x] Handle projection translation to the default for our GTiff import
* [x] Parse subprocess returns
* [x] Give proper warnings when ROI are missing
* [x] Change `Export Mask` to switch properly
* [x] Save the mask as render image with symbology
* [o] Add progress report


## Steps

* Import gtt_0_0.bmp
* Export gtt_0_0 layer (Right click Export > Save As)
  * output mode:    Rendered Image
  * format:         GeoTiff
  * CRS:            EPSG:4326
  * filename:       gtt_satmap.tiff
```cmd
C:\Program Files\QGIS 3.6\bin>gdal_translate.exe -a_srs EPSG:4326 -of GTiff "B:\Arma\Frontline\QGIS_ArmaTerrainTools\Al-Thawrah\gtt_export\gtt_0_0.bmp" "B:\Arma\Frontline\QGIS_ArmaTerrainTools\Al-Thawrah\gtt_export\gtt_export2.tif"


gdal_translate -of GTiff -a_srs EPSG:4326 gtt_0_0.bmp gtt_out.tif -a_ullr 0.0 0.0 4096.0 -4096.0

```


### Processing: otb:ComputeImageStatistics
* Input layer: gtt_satmap (The geotiff one)
* Save as otb_stats.xml

### Create Region of Interests layer
  * Create shapefile layer 'otb_roi' with EPSG:4326
  * Add attribute: type (INT)
  * Draw shapes on areas that should be marked separately
  * Left click to add point, right click to end, type should be set the same for similar classes

### Processing: otb:TrainImagesClassifier
* input image: gtt_satmap2.tiff
* input vector: 'otb_roi' 
* input xml: otb_stats.xml file
* Field name: type
* output model: otb.model
* output confusion matrix: otb_matrix.txt
```py
{ 'io.il' : ['B:/Arma/Frontline/QGIS_ArmaTerrainTools/Al-Thawrah/gtt_export/otb/gtt_satmap.tif'], 'io.vd' : ['B:/Arma/Frontline/QGIS_ArmaTerrainTools/Al-Thawrah/gtt_export/otb_roi3.shp'], 'io.valid' : '', 'io.imstat' : 'B:\\Arma\\Frontline\\QGIS_ArmaTerrainTools\\Al-Thawrah\\gtt_export\\otb\\otb_stats.xml', 'io.out' : 'B:/Arma/Frontline/QGIS_ArmaTerrainTools/Al-Thawrah/gtt_export/otb/otb.model', 'io.confmatout' : 'B:/Arma/Frontline/QGIS_ArmaTerrainTools/Al-Thawrah/gtt_export/otb/otb_matrix.txt', 'cleanup' : True, 'sample.mt' : 1000, 'sample.mv' : 1000, 'sample.bm' : 1, 'sample.vtr' : 0.5, 'sample.vfn' : 'type', 'elev.dem' : '', 'elev.geoid' : '', 'elev.default' : 0, 'classifier' : 'libsvm', 'classifier.libsvm.k' : 'linear', 'classifier.libsvm.m' : 'csvc', 'classifier.libsvm.c' : 1, 'classifier.libsvm.nu' : 0.5, 'classifier.libsvm.opt' : False, 'classifier.libsvm.prob' : False, 'rand' : 0 }
```

### Processing: otb:ImageClassifier
* in: gtt_satmap2.tiff
* model file: otb.model
* statistics file: otb_stats.xml
* output: otb_output
```py
{ 'in' : 'B:/Arma/Frontline/QGIS_ArmaTerrainTools/Al-Thawrah/gtt_export/gtt_satmap3.tif', 'mask' : None, 'model' : 'B:\\Arma\\Frontline\\QGIS_ArmaTerrainTools\\Al-Thawrah\\gtt_export\\otm.model', 'imstat' : 'B:\\Arma\\Frontline\\QGIS_ArmaTerrainTools\\Al-Thawrah\\gtt_export\\image_stats.xml', 'nodatalabel' : 0, 'out' : 'B:/Arma/Frontline/QGIS_ArmaTerrainTools/Al-Thawrah/gtt_export/otm_out.tif', 'confmap' : 'TEMPORARY_OUTPUT', 'outputpixeltype' : 5 }
```