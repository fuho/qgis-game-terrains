"""
    UI functions for classification, mainly related to progress parsing and the widget
"""

from pathlib import Path
from typing import Tuple

from qgis.utils import iface
from PyQt5.QtWidgets import QFileDialog, QProgressBar
from PyQt5.QtCore import QObject, pyqtSignal

from gtt import classification
from gtt.functions import message_log
from . import roi


def initUI(widget):
    widget.button_otb_roi.clicked.connect(roi.create_wizard)


class Progress(QObject):
    """Keeps track of our classification progress"""

    log = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.modifier = 1
        self.handled = []
        self.log.connect(self.log_filter)

    def set_progress_modifer(self, modifier: float):
        """Sets the modifier for the progress. All progress will be multiplied by this value
        
        Args:
            modifier (float): [description]
        """
        self.modifier = modifier

    def set_progress(self, progress: int):
        """Overrides the progress to this percentage"""
        iface.gtt.setProgress(progress)

    def _log(self, text: str):
        """Prints out line and sets progress
        
        Args:
            line (str): Line coming directly out of subprocess
        """
        # return
        message_log(text)

    def check(self, line: str, check: str) -> bool:
        if line.startswith(check):
            progress = self.parse_progress(line)
            if check not in self.handled:

                action_index = line.find("...: ")
                if check == "Writing ":  # Rewrite the full path to only the filename
                    path = line[8:action_index]
                    name = Path(path).stem
                    line = line.replace(path, name)

                self.handled.append(check)
                self._log(line[:action_index])

            if progress >= 0:
                iface.gtt.setProgress(self.modifier * progress)
            return True
        return False

    def log_filter(self, line: str):
        """Filters OrfeoToolbox subprocess prints
        
        Args:
            line (str): original text

        Returns:
            str: String to pass, empty if nothing to pass through
        """
        if self.check(line, "Processing Image"):
            return

        if self.check(line, "Analyze polygons"):
            return

        if self.check(line, "Extracting sample values"):
            return

        if self.check(line, "Split samples between"):
            return

        if self.check(line, "Writing "):
            return

        if line.startswith("Processing ..."):
            message_log(line)
            return

        message_log(line, emit=False)

    def parse_progress(self, line: str) -> int:
        """Parses a OTB subprocess line into a progress percentage
        
        Args:
            line (str): [description]
        
        Returns:
            int: progress percentage
        """

        try:
            index = line.find("...: ")
            if index == -1:
                return -1

            value = line[index + 5 : line.find("%", index)]
            return int(value)
        except ValueError:
            return 0
