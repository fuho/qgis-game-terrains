from .arma import EngineArma
from .ue4 import EngineUE4
from .unity import EngineUnity
from .enfusion import EngineEnfusion

engines = (
    EngineArma(),
    EngineEnfusion(),
    EngineUE4(),
    EngineUnity(),
)
