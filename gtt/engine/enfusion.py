import subprocess
from pathlib import Path
from numpy import loadtxt, savetxt
from configparser import ConfigParser

from qgis.core import QgsTask
from qgis.utils import iface
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from .base import Engine, power_of_two, set_tooltip, export_heightmap_info
from .arma import HeightmapAsc
from gtt.functions import save_location, qgis_bin_folder, message_log


class EngineEnfusion(Engine):
    COORD_X = 0
    COORD_Y = 0
    CRS = "EPSG:32631"
    NAME = "Enfusion"
    SATMAP_FORMAT = ".png"
    MASK_SUPPORT = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def heightResolutionValidate(self, field: QWidget, value: int) -> bool:
        """Checks if the heightmap resolution is valid, will mark the field red if invalid
        
        Args:
            field (QWidget): The widget
            value (int): Value
        
        Returns:
            bool: True if valid
        """

        settings = iface.gtt.settings

        # Mark that cellsize lower than 2 can give issue
        field.__valid = True
        
        cellsize = settings["mapsize"] / value
        if settings["scaled"]:
            cellsize = cellsize * (settings["scaled_size"] / settings["mapsize"])
        
        if cellsize < 2.0:
            field.setStyleSheet("background-color: yellow")
            set_tooltip(
                field, f"Cellsize lower than 2.0 m/px is not recommend due to performance reasons.",
            )
            return True


        # General resolution limitations
        if value > 16384:
            field.setStyleSheet("background-color: yellow")
            set_tooltip(
                field, f"{value}px only for expert users. Don't use this unless you know what you're doing",
            )
            return True

        field.setStyleSheet("")
        set_tooltip(field, "")
        return True

    def heightExport(self, path) -> QgsTask:
        """Converts the resampled heightmap to ASC, and adjust the header for arma mapframe"""
        self.task = HeightmapAscEnfusion(self, "gtt_heightmap", path, iface.gtt.settings)
        return self.task

    def exportLog(self, config: ConfigParser):
        if iface.gtt.settings["export_height"] and self.task:
            section = {}
            section["Minimum height"] = self.task.min_height
            section["Maximum height"] = self.task.max_height
            section["Height range"] = float(self.task.max_height) - float(self.task.min_height)
            section["Terrain Lowered to 0m"] = iface.gtt.settings["lower_to_zero"]
            config[f"## {self.NAME} Heightmap settings ##"] = section

        if iface.gtt.settings["export_roads"]:
            section = {}
            section["X Shift"] = 0
            section["Y Shift"] = 0
            section["ID Column name"] = "ID"

# We'd like to export the known ASC range here
class HeightmapAscEnfusion(HeightmapAsc):
    def __init__(self, engine: Engine, exportname: str, filepath: Path, settings: dict):
        self.engine = engine
        self.min_height = 0
        self.max_height = 0
        super().__init__(exportname, filepath, settings)

    def run(self):
        # Get the heightmap range so we can export it
        self.min_height, self.max_height = export_heightmap_info(self.filepath)
        return super().run()

    def heightmap_arma(self, filepath: Path) -> Path:
        """This handles the exported file according to arma standards"""
        # TODO - Readjust the heightmap to be at reasonble range (If entire terrain is high up, then we can move it all down)'
        # Make a checkbox under the Height tab to "redistribute", in which case we check for lowest number

        try:
            heightmaparray = loadtxt(str(filepath), skiprows=6)
            size = self.settings["height_reso"]
            cellsize = self.settings["mapsize"] / size

            if self.settings["scaled"]:
                cellsize = cellsize * (self.settings["scaled_size"] / self.settings["mapsize"])

            header = "ncols         %s\n" % size
            header += "nrows         %s\n" % size
            header += "xllcorner     %s.000000\n" % self.engine.COORD_X
            header += "yllcorner     %s.000000\n" % self.engine.COORD_Y
            header += "cellsize      %s\n" % cellsize
            header += "NODATA_value  -9999.0"

            # Scale the terrain
            if self.settings["scaled_height"]:
                heightmaparray = heightmaparray * (self.settings["scaled_size"] / self.settings["mapsize"])

            # Offset the entire terrain with the minimum height
            height = int(self.min_height)
            if self.settings["lower_to_zero"] and height > 0:
                heightmaparray = heightmaparray - height

            savetxt(str(filepath), heightmaparray, header=header, fmt="%1.2f", comments="")
        except Exception as e:
            self.error = e
            return None

        return filepath
