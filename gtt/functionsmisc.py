"""Misc functions to do various things, mainly technical stuff called from Plugins menu"""

from math import log as math_log
from qgis.core import QgsSettings, QgsApplication
from qgis.utils import iface

from gtt.static import SETTINGSNAME
from gtt.functions import get_temp_path, message_log, settings_path


def get_tools():
    if hasattr(iface, "gtt"):
        return iface.gtt
    else:
        None


def reset_settings(*args, **kwargs):
    """Removes all QgsSettings related to the tool, then reset them to defaults"""

    settings = QgsSettings()
    settings.remove(SETTINGSNAME)
    tools = get_tools()
    if tools is not None:
        tools.loadSettings()

    iface.messageBar().pushSuccess("GTT", "Reset to default settings")


def clear_cache(*args, **kwargs):
    """ Clears the plugin data folder"""
    deleted = 0
    temp = get_temp_path()
    for file in temp.rglob("*"):
        try:
            (temp / file).unlink()
            deleted += 1
        except Exception as e:
            pass

    if deleted:
        iface.messageBar().pushSuccess("GTT", f"Deleted {deleted} files")
        message_log(f"Deleted {deleted} files")


def pydevd_attach(*args, **kwargs):
    """Create pdb to attach to"""
    try:
        import ptvsd
        ptvsd.enable_attach()
        ptvsd.wait_for_attach()
    except ImportError:
        iface.messageBar().pushCritical("GTT", "ptvsd not installed")



def add_file_logging(filename: str = "gtt.log"):
    """Adds logging to given file from all messages in QgsMessageLog"""
    filepath = settings_path() / filename
    filepath.parent.mkdir(parents=True, exist_ok=True)
    filepath = str(filepath)

    def write_log_message(message, tag, level):
        with open(filepath, "a") as logfile:
            logfile.write(f"{tag}({level}): {message}\n")

    QgsApplication.messageLog().messageReceived.connect(write_log_message)


def get_zoom_level() -> int:
    """Gets zoom level in current canvas used for WMS tile downloading"""
    scale = iface.mapCanvas().scale()
    dpi = iface.mainWindow().physicalDpiX()
    maxScalePerPixel = 156543.04
    inchesPerMeter = 39.37
    zoomlevel = int(round(math_log(((dpi * inchesPerMeter * maxScalePerPixel) / scale), 2), 0))
    return zoomlevel
