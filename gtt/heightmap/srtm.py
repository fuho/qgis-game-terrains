import math
from pathlib import Path

from .source import HeightmapSource, register_heightmap
from gtt.functions import get_temp_path
from gtt.extent import transform_extent
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from gtt.downloader import Download


@register_heightmap
class SrtmHeightmap(HeightmapSource):
    BASEURL = "https://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/"
    NAME = "srtm"

    def download(self, downloader: "Download"):
        """Downloads the DEM files for current marked extent"""

        tiles = self.find_tiles()
        if tiles:
            for tilename in tiles:
                url = f"{self.BASEURL}{tilename}"
                file = self.get_folder(self.NAME) / tilename

                if not Path(file).exists():
                    credentials = (
                        "nasa",
                        "https://urs.earthdata.nasa.gov",
                        "https://urs.earthdata.nasa.gov/users/new",
                    )

                    def callback(filename: str, path: str):  # Get the name for the unpacked file
                        filepath = Path(filename)
                        return str((filepath.parent / filepath.name.split(".")[0]).with_suffix(".hgt"))

                    downloader.get_file(url, file, cleanup=True, credentials=credentials, callback=callback)
                else:
                    downloader.done.emit(str(file).replace("SRTMGL1.", "").replace(".zip", ""))
            return True
        else:
            return False

    def find_tiles(self):
        """
            https://github.com/bopen/elevation/blob/master/elevation/datasource.py
        """
        bounds = transform_extent(targetcrs="EPSG:4326")
        left = int(math.floor(bounds[0]))
        bottom = int(math.floor(bounds[1]))
        right = int(math.ceil(bounds[2]))
        top = int(math.ceil(bounds[3]))

        lat_diff = abs(top - bottom)
        lon_diff = abs(right - bottom)
        n_tiles = lat_diff * lon_diff
        image_counter = 0
        tiles = []

        for lat in range(bottom, top):
            for lon in range(left, right):
                if lon < 10 and lon >= 0:
                    lon_tx = "E00%s" % lon
                elif lon >= 10 and lon < 100:
                    lon_tx = "E0%s" % lon
                elif lon >= 100:
                    lon_tx = "E%s" % lon
                elif lon > -10 and lon < 0:
                    lon_tx = "W00%s" % abs(lon)
                elif lon <= -10 and lon > -100:
                    lon_tx = "W0%s" % abs(lon)
                elif lon <= -100:
                    lon_tx = "W%s" % abs(lon)

                if lat < 10 and lat >= 0:
                    lat_tx = "N0%s" % lat
                elif lat >= 10 and lat < 100:
                    lat_tx = "N%s" % lat
                elif lat > -10 and lat < 0:
                    lat_tx = "S0%s" % abs(lat)
                elif lat < -10 and lat > -100:
                    lat_tx = "S%s" % abs(lat)

                tile = f"{lat_tx}{lon_tx}.SRTMGL1.hgt.zip"
                tiles.append(tile)

        return tiles

    def available(self):
        return True
