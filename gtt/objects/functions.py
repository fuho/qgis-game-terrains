from pathlib import Path

from PyQt5.QtWidgets import QFileDialog
from qgis.utils import iface
from qgis.core import QgsSettings

from gtt.static import SETTINGSNAME
from gtt.functions import project_folder


def ask_library_path(path: Path) -> str:
    location = QFileDialog.getExistingDirectory(
        parent=iface.mainWindow(), caption="Select TML file directory", directory=str(path),
    )
    return location


def select_file() -> Path:
    """Selects BMP file, and turns it into an amazing mask"""
    options = QFileDialog.Options()

    settings = QgsSettings()
    dir_last = settings.value(f"{SETTINGSNAME}/objects/import_path", None, type=str)
    if dir_last == "" or not Path(dir_last).is_dir():
        path = project_folder()
    else:
        path = Path(dir_last)

    data = QFileDialog().getOpenFileName(
        parent=iface.mainWindow(),
        caption="Select object file",
        directory=str(path),
        filter="TB Object file (*{})".format(".txt"),
        options=options,
    )
    file = data[0]
    if file:
        path = Path(file)
        settings.setValue(f"{SETTINGSNAME}/objects/import_path", str(path.parent))
        return path
    return None
