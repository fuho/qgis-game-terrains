import json
from pathlib import Path

from qgis.core import QgsProject, QgsRasterLayer, QgsProject
from gtt.functions import get_layer, set_layer_visibility
from gtt.extent import raise_extent


def add_satmap(source: str):
    """Adds given satmap to the canvas"""

    details = satmap_source(source)
    provider = details["provider"]
    minzoom = details["minzoom"]
    url = details["url"].replace("{minzoom}", str(minzoom))

    currentlayer = get_layer("gtt_satmap")
    if currentlayer is not None:
        QgsProject.instance().removeMapLayers([currentlayer.id()])

    layer = QgsRasterLayer(url, "gtt_satmap", provider)
    QgsProject.instance().addMapLayer(layer)
    raise_extent()


def satmap_source(source: str):
    # Check for available sources
    source_file = Path(__file__).parent / "satmap.json"
    with source_file.open() as json_file:
        data = json.load(json_file)
    try:
        details = data[source]
    except KeyError:
        raise Exception(f"Unknown satmap source: {source}")
    return details


def zoom_source(source: str, zoom: int) -> str:
    """
        Gets zoomed version of satmap, 
        if source has zmin and zmax value replace them by our wanted zoom,
        and create a new layer with it to return, otherwise return the old layer
    """

    def replace(source: str, targets: tuple, value: int):
        parts = source.split("&")  # type: list
        for i, part in enumerate(parts):
            for target in targets:
                if part.startswith(target):
                    parts[i] = f"{target}={value}"
        return "&".join(parts)

    def find_max(source: str):
        parts = source.split("&")  # type: list
        for part in parts:
            if part.startswith("zmax"):
                try:
                    return int(part.split("=")[-1])
                except ValueError:
                    return -1
        return -1

    z_max = find_max(source)
    if z_max != -1:
        zoom_result = min([z_max, zoom])
        source = replace(source, ("zmin", "zmax"), zoom_result)
        return (True, source)
    else:
        return (False, source)


def satmap_zoomed(layer: QgsRasterLayer, zoom: int, name="gtt_zoomed") -> QgsRasterLayer:
    """
        Gets zoomed version of satmap, 
        if source has zmin and zmax value replace them by our wanted zoom,
        and create a new layer with it to return, otherwise return the old layer
    """
    source = layer.source()  # type: str
    zoomed, source = zoom_source(layer.source(), zoom)
    if zoomed:
        current = get_layer(name)
        if current is not None:
            QgsProject.instance().removeMapLayers([current.id()])

        layer = QgsRasterLayer(source, name, "wms")
        QgsProject.instance().addMapLayer(layer)
        set_layer_visibility(layer, False)
        return layer
    else:
        return layer
