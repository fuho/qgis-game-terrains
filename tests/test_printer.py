"""
    Tests the satmap/mask Print layout functions
"""
import unittest
from . import functions

from qgis.utils import iface
from gtt.exporter import satmap


def test_fulldisk_single():
    satmap.export_tile(1, 1, path=functions.full_disk())


def test_fulldisk_all():
    satmap.export_full(path=functions.full_disk())


class TestPrinter(unittest.TestCase):
    def test_fulldisk(self):
        test_fulldisk_single()


if __name__ == "__main__":
    unittest.main()
